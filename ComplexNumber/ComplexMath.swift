//
//  ComplexMath.swift
//  UniCountFrameWork
//
//  Created by Данил Войдилов on 30.04.2018.
//  Copyright © 2018 Данил. All rights reserved.
//

import Foundation

public func toAlg(_ R: Double, A: Double) -> Complex {
    return Complex(R * cosinus(A), i: R * sinus(A))
}

public func root(_ value: Complex, exponent: Complex) -> Complex {
    if exponent != 0 {
        return value ^ (1 / exponent)
    } else {
        return .nan
    }
}

fileprivate func R(_ n: Int) -> Double {
    var res: Double = 1
    if n > 1 {
        for i in 2...n
        {
            res /= Double(i)
        }
    } else if n < 0 {
        for i in n..<0
        {
            res /= Double(i)
        }
    }
    return res
}

public func fact(_ left: Double) -> Complex {
    if left < 0 || floor(left) != left {
        return .nan
    }
    return Complex(1 / R(Int(left)), i: 0)
}

public func percent(_ left: Complex) -> Complex {
    return left / 100
}

public func gamma(_ z: Complex) -> Complex {
    if z.re < 0 {
        return Complex.pi / (sin(Complex.pi * z) * gamma(1 - z))
    }
    var res = 1 / z
    for i in 1...100000 {
        res = res * ((1 + 1 / Complex(i)) ^ z) / (1 + z / Complex(i))
    }
    return res
}

public func fact(_ left: Complex) -> Complex {
    if left.im == 0 && left.re < 0 && left.re == floor(left.re) { return .nan }
    if left.im == 0 && left.re >= 0 && left.re == floor(left.re) { return Complex(1 / R(Int(left.re)), i: 0) }
    return gamma(left + 1)
}

public func conj(_ right: Complex) -> Complex { return Complex(right.re, i: 0 - right.im) }

fileprivate func sinus(_ right: Double) -> Double {
    if right.truncatingRemainder(dividingBy: .pi) == 0 {
        return 0
    }
    return sin(right)
}

fileprivate func cosinus(_ right: Double) -> Double {
    if abs((right / (.pi/2)).truncatingRemainder(dividingBy: 2)) == 1 {
        return 0
    }
    return cos(right)
}

public func tg(_ right: Double) -> Double {
    if right.truncatingRemainder(dividingBy: .pi) == 0 {
        return 0
    }
    if abs((right / (.pi/2)).truncatingRemainder(dividingBy: 2)) == 1 {
        return Double.infinity
    }
    return tan(right)
}

public func cot(_ right: Double) -> Double { return tg(.pi/2 - right) }

public func sin(_ r: Complex) -> Complex {
    if r.im == 0 { return Complex(sinus(r.re), i: 0) }
    return Complex(sinus(r.re) * cosh(r.im), i: cosinus(r.re) * sinh(r.im))
}

public func cos(_ r: Complex) -> Complex {
    if r.im == 0 { return Complex(cosinus(r.re), i: 0) }
    return Complex(cosinus(r.re) * cosh(r.im), i: -sinus(r.re) * sinh(r.im))
}

public func tg(_ right: Complex) -> Complex {
    let a = right.re
    let b = right.im
    if b == 0 { return Complex(tg(a), i: 0) }
    return sin(right) / cos(right)
}

public func acot(_ left: Double) -> Double {
    if left.isInfinite {
        if left.sign == .minus {
            return 0 - .pi / 2
        } else {
            return .pi / 2
        }
    }
    return atan(1 / left)
}

public func coth(_ left: Double) -> Double {
    if tanh(left) == 0 { return Double.infinity }
    return 1 / tanh(left)
}

public func acoth(_ left: Double) -> Double {
    return atanh(1 / left)
}

public func ctg(_ right: Complex) -> Complex {
    let a = right.re
    if right.im == 0 { return Complex(cot(a), i: 0) }
    return cos(right) / sin(right)
}

public func sh(_ right: Complex) -> Complex {
    if right.im == 0 { return Complex(sinh(right.re), i: 0) }
    return 0 - .i * sin(.i * right)//Complex(sinh(right.re) * cosinus(right.im), i: sinus(right.im) * cosh(right.re))
}

public func ch(_ right: Complex) -> Complex {
    if right.im == 0 { return Complex(cosh(right.re), i: 0) }
    return cos(.i * right)//Complex(cosh(right.re) * cosinus(right.im), i: sinus(right.im) * sinh(right.re))
}

public func th(_ right: Complex) -> Complex {
    if right.im == 0 { return Complex(tanh(right.re), i: 0) }
    return sh(right) / ch(right)
}

public func cth(_ right: Complex) -> Complex {
    if right.im == 0 {
        if right.re != 0 && right.re != .pi {
            return Complex(1 / tanh(right.re), i: 0)
        }
    }
    return ch(right) / sh(right)
}

public func asin(_ right: Complex) -> Complex {
    if right.im == 0 && abs(right.re) <= 1 { return Complex(asin(right.re), i: 0) }
    return 0 - .i * ln(.i * right + √(1 - right * right))
}

public func acos(_ right: Complex) -> Complex {
    if right.im == 0 && abs(right.re) <= 1 { return Complex(acos(right.re), i: 0) }
    return .pi/2 + .i * ln(.i * right + √(1 - right * right))
}

public func atg(_ right: Complex) -> Complex {
    let a = right.re
    if right.im == 0 {
        if right.re.isInfinite {
            if right.re.sign == .minus {
                return Complex(0 - .pi / 2, i: 0)
            } else {
                return Complex(.pi / 2, i: 0)
            }
        }
        return Complex(atan(a), i: 0)
    }
    return .i * (ln(1 - .i * right) - ln(1 + .i * right)) / 2
}

public func actg(_ right: Complex) -> Complex {
    if right.im == 0 { return Complex(acot(right.re), i: 0) }
    return .i * (ln((right - .i) / right) - ln((right + .i) / right)) / 2
}

public func ash(_ right: Complex) -> Complex {
    if right.im == 0 {
        return Complex(asinh(right.re), i: 0)
    }
    return ln(right + √(right * right  + 1))
}

public func ach(_ right: Complex) -> Complex {
    if right.im == 0 && right.re >= 1 {
        return Complex(acosh(right.re), i: 0)
    }
    return ln(right + √(right * right - 1))
}

public func ath(_ right: Complex) -> Complex {
    if right.im == 0 && abs(right.re) <= 1 {
        return Complex(atanh(right.re), i: 0)
    }
    return ln((1 + right) / (1 - right)) / 2
}

public func acth(_ right: Complex) -> Complex {
    if right.im == 0 && abs(right.re) >= 1 {
        return Complex(acoth(right.re), i: 0)
    }
    return ln((1 + right) / (right - 1)) / 2
}

public func log(_ left: Complex, right: Complex) -> Complex {
    guard left != 1 else { return .nan }
    if right.im == 0 && left.im == 0 && right.re > 0  && left.re > 0 {
        return Complex(log2(right.re) / log2(left.re), i: 0)
    }
    return ln(right) / ln(left)
}

public func ln(_ right: Complex) -> Complex {
    if right.im == 0 && right.re > 0 {
        return Complex(log(right.re), i: 0)
    }
    if right.magnitude <= 0 { return .nan }
    return Complex(log(right.magnitude), i: right.exponent)
}

public func lg(_ right: Complex) -> Complex {
    if right.im == 0 && right.re > 0 { return Complex(log10(right.re), i: 0) }
    return log(10, right: right)
}

public func exp(_ right: Complex) -> Complex {
    if right.im == 0 { return Complex(exp(right.re), i: 0) }
    return Complex(exp(right.re) * cosinus(right.im), i: exp(right.re) * sinus(right.im))
}

public func abs(_ right: Complex) -> Double {
    return right.magnitude
}
