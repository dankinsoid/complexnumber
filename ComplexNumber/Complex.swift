//  Complex.swift
//  Advanced calculator
//
//  Created by Данил on 04.04.15.
//  Copyright (c) 2015 Данил. All rights reserved.
//

import Foundation

prefix operator ~
prefix operator √
precedencegroup PowerPrecedence { higherThan: MultiplicationPrecedence }
infix operator √: PowerPrecedence

public struct Complex: Numeric, CustomStringConvertible, Codable, ExpressibleByFloatLiteral {
    public var description: String {
        guard im != 0 else { return String(format: "%g", re) }
        var imstr = String(format: "%g", im) + "i"
        if abs(im) == 1 { imstr = im < 0 ? "-i" : "i" }
        guard self.re != 0 else { return imstr }
        return String(format: "%g", re) + (self.im < 0 ? "" : "+") + imstr
    }
    
    public typealias FloatLiteralType = Double
    public typealias IntegerLiteralType = Int
    public typealias Magnitude = Double
    
    public var re: Double
    public var im: Double
    
    public var angle: Double {
        get {
            var result = acos(re / magnitude)
            if im < 0 { result += .pi }
            return result.truncatingRemainder(dividingBy: 2 * .pi)
        }
        set {
            let ampl = magnitude
            re = cos(newValue) * ampl
            im = sin(newValue) * ampl
        }
    }
    
    public var exponent: Double {
        get { return angle }
        set { angle = newValue }
    }
    
    public var magnitude: Double {
        get {
            if im == 0 {
                return abs(re)
            } else if re == 0 {
                return abs(im)
            }
            return sqrt(re * re + im * im)
        }
        set {
            let angl = angle
            re = cos(angl) * newValue
            im = sin(angl) * newValue
        }
    }
    public static let i = Complex(i: 1)
    public static let nan = Complex(Double.nan, i: Double.nan)
    public static let zero = Complex(0, i: 0)
    public static let infinity = Complex(Double.infinity, i: Double.infinity)
    public static let pi = Complex(Double.pi, i: 0)
    
    public init(floatLiteral value: FloatLiteralType) {
        re = value
        im = 0
    }
    
    public init(integerLiteral value: IntegerLiteralType) {
        re = Double(value)
        im = 0
    }
    
    public init?<T>(exactly source: T) where T : BinaryInteger {
        guard let d = Double(exactly: source) else { return nil }
        re = d
        im = 0
    }
    
    public init(_ source: Int) {
        re = Double(source)
        im = 0
    }
    
    public init?(_ string: String) {
        let s = string.replacingOccurrences(of: " ", with: "")
        if let d = Double(string) {
            self.init(d, i: 0)
            return
        }
        guard s.contains("i") && s.components(separatedBy: "i").count == 2 else { return nil }
        let s1 = s.replacingOccurrences(of: "i", with: "")
        if let d = Double(s1) {
            self.init(0, i: d)
            return
        } else {
            let pos = s.index(after: s.startIndex)
            if let range = s.rangeOfCharacter(from: CharacterSet(["+", "-"]), range: pos..<s.endIndex) {
                if let f = Complex(String(s[s.startIndex..<range.lowerBound])),
                    let d = Complex(String(s[range.lowerBound..<s.endIndex])) {
                    self.init(f.re + d.re, i: f.im + d.im)
                    return
                }
            }
        }
        return nil
    }
    
    public init(_ re: Double = 0, i: Double = 0) {
        self.re = re
        self.im = i
    }
    
    public init(magnitude: Double, angle: Double) {
        self.re = cos(angle) * magnitude
        self.im = sin(angle) * magnitude
    }
    
    public init(magnitude: Double, exponent: Double) {
        self.init(magnitude: magnitude, angle: exponent)
    }
    
    public func toExp() -> (magnitude: Double, exponent: Double) {
        if re != 0 {
            if re > 0 {
                return (abs(self), atan(im / re))
            } else {
                return (abs(self), .pi + atan(im / re))
            }
        } else if im > 0 {
            return (abs(self), .pi / 2)
        } else {
            return (abs(self), 0 - .pi / 2)
        }
    }
    
    public static func ==(lhs: Complex, rhs: Complex) -> Bool { return lhs.im == rhs.im && lhs.re == rhs.re }
    public static func *=(lhs: inout Complex, rhs: Complex) { lhs = lhs * rhs }
    public static func /=(lhs: inout Complex, rhs: Complex) { lhs = lhs / rhs }
    public static func +=(lhs: inout Complex, rhs: Complex) { lhs = lhs + rhs }
    public static func -=(lhs: inout Complex, rhs: Complex) { lhs = lhs - rhs }
    public static func +(left: Complex, right: Complex) -> Complex { return Complex(left.re + right.re, i: left.im + right.im) }
    public static func -(left: Complex, right: Complex) -> Complex { return Complex(left.re - right.re, i: left.im - right.im) }
    
    public static func *(left: Complex, right: Complex) -> Complex {
        if (left == .infinity || right == .infinity) && left != 0 && right != 0 {
            return .infinity
        }
        return Complex(left.re * right.re - left.im * right.im, i: left.re * right.im + left.im * right.re)
    }
    
    public static func /(left: Complex, right: Complex) -> Complex {
        if right == 0 && left != 0 {
            return .infinity
        }
        if (right.re == Double.infinity || right.im == Double.infinity) && left.re != Double.infinity && left.im != Double.infinity {
            return 0
        }
        let den = right.re * right.re + right.im * right.im
        return Complex((left.re * right.re + left.im * right.im) / den, i: (left.im * right.re - left.re * right.im) / den)
    }
    
    public static func ^(left: Complex, right: Complex) -> Complex {
        if left == 0 { return 0 }
        if right == 0 { return 1 }
        let a = left.toExp()
        return toAlg(pow(a.magnitude, right.re) * pow(M_E, 0 - a.exponent * right.im), A: log(a.magnitude) * right.im + a.exponent * right.re)
    }
    public static prefix func ~(right: Complex) -> Complex { return conj(right) }
    public static prefix func √(right: Complex) -> Complex { return root(right, exponent: 2) }
    public static func √(left: Complex, right: Complex) -> Complex { return root(right, exponent: left) }
    
}

